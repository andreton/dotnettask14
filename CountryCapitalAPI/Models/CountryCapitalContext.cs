using Microsoft.EntityFrameworkCore;
namespace CountryCapitalAPI.Models{

    public class CountryCapitalContext :DbContext
    {
        /*DB-Context*/
        public CountryCapitalContext(DbContextOptions<CountryCapitalContext> options): base(options){

        }
        public DbSet<CountryCapital> CountryCapitalItems{get;set;}
    }
}