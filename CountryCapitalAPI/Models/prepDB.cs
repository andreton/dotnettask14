using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace CountryCapitalAPI.Models
{
    public class PrepDB
    {
        /*Function used for populate the database, this is usefull when spinning up a container */
        public static void PrePopulate(IApplicationBuilder app){
            using (var servicescope=app.ApplicationServices.CreateScope()){
                SeedData(servicescope.ServiceProvider.GetService<CountryCapitalContext>());
            }
        }
        public static void SeedData(CountryCapitalContext context){
                System.Console.WriteLine( "Applying migrations");
                context.Database.Migrate();
                if(!context.CountryCapitalItems.Any()){
                    System.Console.WriteLine( "Adding values");
                    context.CountryCapitalItems.AddRange(
                    new CountryCapital(){Country="Norway",Capital="Oslo"},
                     new CountryCapital(){Country="Sweden",Capital="Stockholm"},
                        new CountryCapital(){Country="Denmark",Capital="Københaven"}
                    );
                    context.SaveChanges();
                }
                else 
                {
                System.Console.WriteLine( "Applying migrations");
                }

        }
    }
}