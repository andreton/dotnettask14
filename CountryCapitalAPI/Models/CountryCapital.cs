namespace CountryCapitalAPI.Models{
    public class CountryCapital
    {
        /*Simple class where the API should return the country and the capital with it */
        public int Id { get; set; }
        public string Country { get; set; }
        public string Capital{get;set;}
    }
}