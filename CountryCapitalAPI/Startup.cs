using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CountryCapitalAPI.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CountryCapitalAPI
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
                        services.AddMvc(option => option.EnableEndpointRouting = false);
                         services.AddDbContext<CountryCapitalContext>
                         //Manually add the database string, server instance is db
                         //(opt=>opt.UseSqlServer(@"Server=db;Database=master;User=sa;Password=Your_password123;"));
                         (opt=>opt.UseSqlServer(@"Server=db;Database=countrycapital;User=sa;Password=MittPassord12345;"));
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            PrepDB.PrePopulate(app);
        }
    }
}
