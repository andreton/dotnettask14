using System.ComponentModel.DataAnnotations;

namespace CountryCapitalAPI.DTO{
    public class CountryCapitalCreateDto
    {
        /*DTO for creating countrycapital without Id*/
       [Required]
       [MaxLength(250)]
        public string Country { get; set; }
         [Required]
       [MaxLength(250)]
        public string Capital { get; set; }
        
    }
}