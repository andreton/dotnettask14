namespace CountryCapitalAPI.DTO{   
     public class CountryCapitalDto
    {
        /*Full DTO with ID*/
        public int Id { get; set; }
        public string Country { get; set; }
        public string Capital { get; set; }
    }
}