using System.ComponentModel.DataAnnotations;

namespace CountryCapitalAPI.DTO{ 
     public class CountryCapitalUpdateDto
    {
      /*DTO to be used with updating values*/
         [Required]
       [MaxLength(250)]
        public string Country { get; set; }
         [Required]
       [MaxLength(250)]
        public string Capital { get; set; }
        
    }
}