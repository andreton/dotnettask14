﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CountryCapitalAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CountryCapitalAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountryCapitalController : ControllerBase
    {
         private readonly CountryCapitalContext _context;
        public CountryCapitalController(CountryCapitalContext context){
                    _context=context;  
        }
        //Get all country and capitals
          [HttpGet]
        public ActionResult<IEnumerable<CountryCapital>>GetCountryCapital(){
            return _context.CountryCapitalItems;
        }

        //Get country and capital by Id
        [HttpGet("{id}")]
        public ActionResult<CountryCapital>GetCountryCapitalById(int id){
            return _context.CountryCapitalItems.Find(id);
        }

        //Put countrycapital in by Id
        [HttpPut("{id}")]
        public ActionResult<IActionResult> PutActor(int id, CountryCapital countryCapital)
        {
            if (id != countryCapital.Id)
            {
                return BadRequest();
            }

            _context.Entry(countryCapital).State = EntityState.Modified;

            try
            {
                 _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryCapitalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        //Http post method
        [HttpPost]
        public ActionResult<CountryCapital> PostCountryCapital(CountryCapital countryCapital)
        {
            _context.CountryCapitalItems.Add(countryCapital);
            _context.SaveChangesAsync();

            return CreatedAtAction("GetCountryCapital", new { id = countryCapital.Id }, countryCapital);
        }

        // DELETE: api/Actors/5
        [HttpDelete("{id}")]
        public  ActionResult<CountryCapital> DeleteCountryCapital(int id)
        {
            var countryCapital =  _context.CountryCapitalItems.Find(id);
            if (countryCapital == null)
            {
                return NotFound();
            }

            _context.CountryCapitalItems.Remove(countryCapital);
            _context.SaveChanges();

            return countryCapital;
        }
         private bool CountryCapitalExists(int id)
        {
            return _context.CountryCapitalItems.Any(e => e.Id == id);
        }

        //

    }
}
